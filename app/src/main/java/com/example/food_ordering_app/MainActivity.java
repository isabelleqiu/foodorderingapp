package com.example.food_ordering_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Get the linear layout XML container object into which we'll be adding our cuisine list fragments
        LinearLayout linearLayoutContainer = findViewById(R.id.cuisine_list_fragment_container_linear_layout);

        //Get linear layout menu bar
        LinearLayout menuBar = findViewById(R.id.menu_bar);

        //FragmentManager is the mechanism for adding fragments programmatically
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        //Loop through all cuisines in menu
        HashMap<String, ArrayList<Dish>> dishesByCuisine = Menu.getInstance().dishesByCuisine();
        for (Map.Entry<String, ArrayList<Dish>> entry : dishesByCuisine.entrySet()) {
            //Get the cuisine enum for this cuisine (dish category)
            Cuisine cuisine = Cuisine.valueOf(entry.getKey());

            //Create the fragment that will be displaying this cuisine
            Fragment fragment = CuisineListFragment.newInstance(cuisine);

            //Add the fragment to our linear layout container
            fragmentTransaction.add(linearLayoutContainer.getId(), fragment, null);
        }

        //End the fragment transaction
        fragmentTransaction.commit();


    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("Resuming ActivityMain");
        invalidateOptionsMenu();  // Refresh action bar button (cart button)
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        // Inflate our action bar layout (action_bar_menu.xml)
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_menu, menu);

        // Check to see if there are items in the cart
        Boolean areItemsInCart = Cart.getInstance().numberOfItems() > 0;

        // Set the cart icon to be enabled/disabled depending on whether there are items in the cart
        menu.findItem(R.id.action_cart).setEnabled(areItemsInCart);

        // Return our inflated menu
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cart:
                System.out.println("Selected cart");
                // Navigate to cart screen
                navigateToCart();
                break;
            default:
                break;
        }
        return true;
    }

    // Private methods
    private void navigateToCart() {
        Intent intent = new Intent(this, CartActivity.class);
        startActivity(intent);
    }

    public void homeButtonPressed() {
        // Return to home screen
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
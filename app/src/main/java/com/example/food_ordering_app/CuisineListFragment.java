package com.example.food_ordering_app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CuisineListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CuisineListFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //Constructor
    public CuisineListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param cuisine The cuisine to display for this cuisine list fragment.
     * @return A new instance of fragment CuisineListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CuisineListFragment newInstance(Cuisine cuisine) {
        CuisineListFragment fragment = new CuisineListFragment();

        //Create a bundle and put in a passed parameter (cuisine name)
        Bundle args = new Bundle();
        args.putString(CUISINE_NAME, cuisine.name());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //Retrieve cuisine name from the bundle (passed in parameter)
            String cuisineName = getArguments().getString(CUISINE_NAME);

            //Get cuisine enum object from cuisine name
            Cuisine cuisine = Cuisine.valueOf(cuisineName);

            //Get all dishes from the menu for this particular cuisine
            ArrayList<Dish> dishes = Menu.getInstance().dishesByCuisine().get(cuisineName);

            // Save the cuisine and the list of dishes
            this.cuisine = cuisine;
            this.dishes = dishes;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_cuisine_list, container, false);

        setupCuisineName();
        setupRecyclerView();

        return rootView;
    }

    //Private constants
    private static final String CUISINE_NAME = "cuisine_name";

    //Private properties
    private Cuisine cuisine;
    private ArrayList<Dish> dishes;

    //Private XML Views
    private View rootView;
    private RecyclerView recyclerView;

    //Private methods
    private void setupCuisineName() {
        //Links the text view from the layout file to a Java object
        TextView cuisineNameTextView = rootView.findViewById(R.id.cuisine_name_text_view);
        cuisineNameTextView.setText(cuisine.toString());
    }

    private void setupRecyclerView() {
        //Links the recycler view from the layout file to a Java object
        recyclerView = rootView.findViewById(R.id.recycler_view);

        //Create linear layout manager for recycler view
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.HORIZONTAL);
        recyclerView.setLayoutManager(layoutManager);

        //Set the adapter for the recycler view
        DishAdapter dishAdapter = new DishAdapter(getContext(), dishes);
        recyclerView.setAdapter(dishAdapter);
    }
    private void userTappedOnDishAtPosition(int position) {

        Dish dish = Menu.getInstance().dishesByCuisine().get(cuisine.name()).get(position);
        System.out.println("User tapped on " + dish.name);

        // Navigate to the Dish details screen
        Intent intent = new Intent(getContext(), DishActivity.class);
        getActivity().startActivity(intent);

        // Save the selected cuisine and dish so we can add them to the Cart later in case user decides to
        Menu.getInstance().selectedDishCuisine = cuisine;
        Menu.getInstance().selectedDishPosition = position;
    }
    // Adapter for Dish objects list in recycler
    private class DishAdapter extends RecyclerView.Adapter<DishAdapter.DishViewHolder> {

        public DishAdapter(@NonNull Context context, @NonNull ArrayList<Dish> dishes) {
            this.context = context;
            this.dishes = dishes;
        }

        @NonNull
        @Override
        public DishViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View itemView = inflater.inflate(R.layout.item_dish, parent, false);
            DishViewHolder viewHolder = new DishViewHolder(itemView);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull DishViewHolder holder, final int position) {
            Dish dish = dishes.get(position);
            holder.nameTextView.setText(dish.name);
            Context context = getContext();
            int id = context.getResources().getIdentifier(dish.imageResourceName, "drawable", context.getPackageName());
            holder.imageView.setImageResource(id);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    userTappedOnDishAtPosition(position);
                }
            });
        }

        @Override
        public int getItemCount() {
            return dishes.size();
        }

        //Private properties
        private final Context context;
        private final ArrayList<Dish> dishes;

        //Dish ViewHolder
        private class DishViewHolder extends RecyclerView.ViewHolder {

            public View itemView;
            public ImageView imageView;
            public TextView nameTextView;

            public DishViewHolder(@NonNull View itemView) {
                super(itemView);
                this.itemView = itemView;
                imageView = itemView.findViewById(R.id.dishImageView);
                nameTextView = itemView.findViewById(R.id.nameTextView);
            }
        }
    }
}